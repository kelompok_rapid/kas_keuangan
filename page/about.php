<?php 
require_once '../koneksi/conn.php'; 
?>
<div class="container-fluid">
    <div class="row bg-title">
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <h4 class="page-title">Tentang Pengembang</h4> </div>
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
            <!-- <ol class="breadcrumb">
                <li><a href="#">Kategori Pengeluaran</a></li>
            </ol> -->
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <div class="row">
        <div class="col-sm-12">
            <div class="white-box">
                <div class="row">
                    <div class="col-sm-12">
                        <h3 class="box-title">Tujuan Pengembang</h3>
                    </div>
                    <div class="col-sm-12">
                        <p>- Untuk mempermudah dalam pengelolaan KAS Keuangan Masjid Nurul Huda di Pasar Minggu.</p>
                        <p>- Sebagai salah satu syarat kelulusan mata kuliah Pengembangan Aplikasi Berbasis RAPID.</p>
                    </div>
                    
                </div>
                
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-12">
            <div class="white-box">
                <div class="row">
                    <div>
                        <h3 class="box-title">Nama Kelompok</h3>
                    </div>
                    <div class="table-responsive"  class="col-md-12">
                        <table class="table" id="dataku">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>NIM</th>                                
                                    <th>Nama</th>
                                    <th>Alamat Email</th>                                
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>1</td>
                                    <td>41517110013</td>
                                    <td>Angga Fauzi</td>
                                    <td>angga.fauzi23@gmail.com</td>
                                </tr>
                                <tr>
                                    <td>2</td>
                                    <td>41517110020</td>
                                    <td>Muhammad Ikhwanudin</td>
                                    <td>m.ikhwanudin777@gmail.com</td>
                                </tr>
                                <tr>
                                    <td>3</td>
                                    <td>41517110084</td>
                                    <td>Muhammad Fikri</td>
                                    <td>muhammadfikrikja@gmail.com</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-12">
            <div class="white-box">
                <div class="row">
                    <div class="col-sm-6">
                        <h3 class="box-title"></h3>
                        <h5>Teknik Informatika - Universitas Mercu Buana </h5>
                    </div>
                </div>
                
            </div>
        </div>
    </div>
    <!-- /.row -->
</div>
<!-- modal -->

<script>

</script>