<?php 
require_once '../../koneksi/conn.php';
// $currentPassword = $conn->real_escape_string($_POST['currentPassword']);
$newPassword = $conn->real_escape_string($_POST['newPassword']);
$confirmPassword = $conn->real_escape_string($_POST['confirmPassword']);

$data = array();
$data['error_string'] = array();
$data['inputerror'] = array();
$data['status'] = TRUE;

// if($nama == ''){
// 	$data['inputerror'][] = 'currentPassword';
// 	$data['error_string'][] = 'Password Saat ini wajib di isi';
// 	$data['status'] = FALSE;
// }

if($newPassword == ''){
	$data['inputerror'][] = 'newPassword';
	$data['error_string'][] = 'Masukkan Password Baru';
	$data['status'] = FALSE;
}

if($confirmPassword == ''){
	$data['inputerror'][] = 'confirmPassword';
	$data['error_string'][] = 'Konfirmasi Password wajib diisi';
	$data['status'] = FALSE;
}

if($data['status'] === FALSE){
	echo json_encode($data);
	exit();
}

$hash_pass = password_hash($confirmPassword, PASSWORD_DEFAULT);

$sql=$conn->query("UPDATE tbl_user SET password='$hash_pass'");
if ($sql) {
    echo json_encode(array("status" => TRUE));
}
?>